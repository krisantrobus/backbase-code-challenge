# Angular Weather

## Running the App

This is a standard angular app with no extra dependencies to build angular libraries etc. It sues Node Package Manager for dependency management, just run `npm i && ng serve`

## What's Included

- Services
  - weather-data.service.ts
- Lazy Loading
- Inline Template, loading component
- Inputs & Outputs
- Directives
  - ngIf
  - ngFor
  - Custom directive - appCardHover _see below_
- Reusable Components & Directives
  - By having its own module and exporting the component to other modules
- View Child _see below_
- Observables & Subscribe
- Animations _see below_
- NgRx state management
  - Both as features and root
  - includes [selectors](./src/app/modules/weather-module/store/selectors/weather.selector.ts)
- Typescript Class [model.ts](./src/app/modules/weather-module/model.ts)

## What's not Included

- Custom Angular library. I have previously done this, complete with including multiple dependencies to ship across applications.
- E2E testing
- Global loading component with states (show loading when calling API)

## Key Decisions

### State Management

An application of this size does not require the use of complex state management but it has been included as a demonstration of capabilities.

State management has been used in the application. It follows the Redux pattern, implemented by NgRx. This was implemented as state management is crucial in scaling sing page applications. By using a state management library it allows new states and component states to be easily integrated and controlled.

This library provides a way to store state into one single, local, state container. It also manages the state of the data in a one way data flow

### Component Driven

As each weather object has the same data structure a stateless card component has been created. This is reused on the /weather/all home page to render each weather card. Integration is with the `@input()` and `@Output` decorator. Also utilising the `ngIf` directive

While the output captures and emits a click function it is just as easy to achieve this with the click of the `<weather-card>` DOM. I have not done this to demonstrate the ability to output from components.

```
backbase-code-challenge\src\app\modules\weather-module\pages\weather-home\weather-home.component.html

<weather-card
    *ngFor="let city of citiesData"
    [city]="city"
    (toggleModal)="toggleModal(city)"
></weather-card>
```

### Typscript

Typscript has been heavily used to ensure that the application is strongly typed. This gives many advantages such as utilising [private and public variables](./src/app/modules/weather-module/components/city-modal/city-modal.component.ts) and methods and defining [interfaces](./src/app/modules/weather-module/model.ts) to define data structures.

### Bootstrap

Bootstrap was implemented as a simple way to style components and control the layout of the application. Examples include using the flex css features to correctly size and arrange content. It's also used to control the colours and theme the application.

Some commonly used classes:

- container, row & col
- d-flex
- bg-\${color}
- justify-content

The use of media queries have also been included to create a highly responsive app fit for both web and mobile. I would encourage you to test both usecases.

## [Weather Data Service](./src/app/modules/weather-module/weather-data.service.ts)

The data service uses the HttpClient provided by Angular. I return Observables back to component and subscribe to retrieve the values.

I would usually on error use another service to provide feedback to the user but I've left that out of scope for this app.

```
backbase-code-challenge\src\app\modules\weather-module\weather-data.service.ts

public getCurrentGroupWeather(): Observable<any> {
    return this.http.get(
        `${this._apiUrl}/group?id=2643743,2653822,2988507,6545310,2759794&units=metric&${this._apiKey}`
    );
}

public getHourlyDataForCityId(cityId: number): Observable<any> {
    return this.http.get(
        `${this._apiUrl}/forecast?id=${cityId}&units=metric&${this._apiKey}`
    );
}
```

The decision to call the api as soon as the app loads was made as the data is common across all pages and is subscribed from the same NgRx store. One the api returns successful a dispatch is made to the store to update the correct feature.

The similar proccess is replicated for querying for the forecast of a city on click of the modal.

```
backbase-code-challenge\src\app\app.component.ts

ngOnInit() {
    this.wdataServ.getCurrentGroupWeather().subscribe(
        data => {
            this._store.dispatch({
                type: WeatherActions.UPDATE_CITIES,
                payload: <Weather[]>data.list
            });
        },
        err => console.log(`error contating api: `, err)
    );
}
```

## NgRx

NgRx has been implemented as the state management for this application. IT acts as the single truth for all state for the application.

### What's been Used:

- Store
- Reducers
- Custom Selectors

### What's not been used:

- Effects
- Actions

Usually I would use actions to handle any asynchronous calls to any APIs but in this scenario I opted for a data service to handle all of this and handle the result in the component on success. I've decided to do it this way to demonstrate knowledge of Angular Services.

### The State

The state of the application has been strongly typed with use of interfaces and initial states. Below are the snippets of code used to define this:

```
backbase-code-challenge\src\app\modules\weather-module\store\state\weather.state.ts

export interface WeatherState {
    cities: Weather[];
    selectedCity: Weather;
    selectedCityForecast: WeatherForecast;
}

export const initialWeatherState: WeatherState = {
    cities: null,
    selectedCity: null,
    selectedCityForecast: null
};

```

```
backbase-code-challenge\src\app\store\app.state.ts

export interface AppState {
    weather: any,
    loading: boolean,
}

export const initialAppState: AppState = {
    weather: initialWeatherState,
    loading: false,
}
```

All weather components use a specific `weatherReducer` which handles all interaction with the `weather` segment/feature. This is achieved by defining the following in the modules of a component:

```
@NgModule({
    imports: [
        ...
        StoreModule.forFeature('weather', weatherReducer)
    ],
})
```

### Reducers

The root reducer was created to ahndle to root of teh state and would handle any operational state such as loading. The use of the root reducer wasn't implenented in teh scope but a `weatherReducer` has been cerated to handle all weather data

```
backbase-code-challenge\src\app\modules\weather-module\store\reducers\weather.reducer.ts

export function weatherReducer(
    state: Object = initialAppState,
    action: Action
) {
  switch (action.type) {
    case WeatherActions.SELECT_CITY:
        return { ...state, selectedCity: action.payload };
    case WeatherActions.UPDATE_FORECAST:
        return { ...state, selectedCityForecast: action.payload };
    case WeatherActions.UPDATE_CITIES:
        return { ...state, cities: action.payload };
    default:
        return state;
  }
}
```

### Selectors

Custom selectors have been defined for making it easier for components to subscribe to specific parts of state. This also allows for consistency across all data retrieved by components as any data manipulation has already occurred.

There are 2#3 examples of this. The first is selecting all the data for 5 cities. The second retrieves the currently selected city. The third is for selecting the forecast only for the current day.

```
backbase-code-challenge\src\app\modules\weather-module\store\selectors\weather.selector.ts

// Select the weather segment of data from the root state to use in selectors
const selectWeather = (state: AppState) => state.weather;

// Retrive the city data
export const getCitiesList = createSelector(
    selectWeather,
    (state: WeatherState) => state.cities
);

// Retrive teh selected City data
export const getSelectedCity = createSelector(
    selectWeather,
    (state: WeatherState) => state.selectedCity
);

export const getSelectedCityForecastCurrentDay = createSelector(
    selectWeather,
    (state: WeatherState) => {
        if (state.selectedCityForecast) {
            //Assemble a similar string to do acomparison to filter the list array
            // for only the current day
            const currentDay = new Date();
            const year = currentDay.getFullYear(),
                month = ("0" + (currentDay.getMonth() + 1)).slice(-2),
                day = ("0" + currentDay.getDate()).slice(-2);

            const currentDayForecast: ForecastObj[] = state.selectedCityForecast.list.filter(
                (forecast: ForecastObj) => {
                // Only return current day forecast objects
                return (
                    forecast.dt_txt.slice(0, 10) === `${year}-${month}-${day}` &&
                    forecast
                );
                }
            );

            return { ...state.selectedCityForecast, list: currentDayForecast };
        }

        return state.selectedCityForecast;
    }
);
```

### Use in Components

The selectors are used in the components to subscribe to the state. All subscriptions are unsubscribed on destroy of the component to avoid memory leaks in the application:

```
backbase-code-challenge\src\app\modules\weather-module\pages\weather-home\weather-home.component.ts

sub: Subscription = null;

ngOnInit() {
    // TODO: unsubsrcibe strategy implementation
    this.sub = this._store.select(getCitiesList).subscribe(cities => {
        this.citiesData = cities;
    });
}

ngOnDestroy() {
    this.sub.unsubscribe();
}
```

### Stringly Typed

I've ensured that all dispatches are strongly typed by using an enumerator to define all types that can occur:

```
backbase-code-challenge\src\app\modules\weather-module\store\types.ts

export enum WeatherActions {
    SELECT_CITY = "SELECT_CITY",
    UPDATE_CITIES = "UPDATE_CITIES",
    UPDATE_FORECAST = "UPDATE_FORECAST"
}
```

## Custom Directive - cardHover

A custom directive had been created to allow for hover styling on the weather cards. It can be applied to any element to apply a property to move card up (top: -10px) and cursor pointer stylings. It has its own module, exporting itself to make it available across the whole application.

The directive acheives this by using host listener decorators to aply and remove styling using a highligh method

```
backbase-code-challenge\src\app\shared\directives\card-hover.directive.ts

@HostListener("mouseenter") onMouseEnter() {
    this.highlight(true);
}

@HostListener("mouseleave") onMouseLeave() {
    this.highlight(false);
}

private highlight(active: boolean): void {
    this.el.nativeElement.style.top = active ? "-10px" : "0";
    this.el.nativeElement.style.cursor = active ? "pointer" : "none";
}
```

It's then added to the weather card compoent:

```
backbase-code-challenge\src\app\modules\weather-module\components\weather-card\weather-card.component.html

<div class="city-card card" *ngIf="city" cardHover>...</div>
```

## View Child

the `@ViewChild` decorator has been used to toggle the state of teh modal. On click in the parent component the toggleShow() method is called. This switches and active boolean which is used to determine the state.

First it is defined in the parent:

```
backbase-code-challenge\src\app\modules\weather-module\pages\weather-home\weather-home.component.html

...
    <weather-city-modal #cityModal></weather-city-modal>
...
```

Then I've defined the view child in the .ts file and called the method on a click function:

```
backbase-code-challenge\src\app\modules\weather-module\pages\weather-home\weather-home.component.ts

export class WeatherHomeComponent implements OnInit {
  @ViewChild('cityModal') private cityModal: CityModalComponent

...

    toggleModal(city: Weather) {
        this._store.dispatch({
            type: WeatherActions.SELECT_CITY,
            payload: city
        });

        ...

        this.cityModal.toggleShow();
    }
```

## Animations

Angular animations have been used on the modal. It is used to fade in and out by changing both the visibility and opacity style. This is achieved by defining the trigger on the element and using an advice boolean.

```
backbase-code-challenge\src\app\modules\weather-module\components\city-modal\city-modal.component.html

<div class="card container-fluid" id="modal-container" [@openClose]="this.active ? 'open' : 'closed'">
```

Next I've defined the animation in the `@Component` decorator:

```
@Component({
    selector: "weather-city-modal",
    templateUrl: "./city-modal.component.html",
    styleUrls: ["./city-modal.component.scss"],
    animations: [
    trigger('openClose', [
        state('open', style({
            visibility: 'visible',
            opacity: 1
        })),
        state('closed', style({
            visibility: 'hidden',
            opacity: 0
        })),
        transition('open => *', [
            animate(200)
        ]),
        transition('closed => open', [
            animate(200)
        ]),
    ])
]
})
```

## Testing

**Links only work in IDE not in browser**

Some basic unit testing has been demonstrated in both the components:

### [WeatherCardComponent](./src/app/modules/weather-module/components/weather-card/weather-card.component.spec.ts)

Simple test to pass in a dummy input object and check that the title has rendered correctly

```
interview-challenge-angular\src\app\modules\weather-module\components\weather-card\weather-card.component.spec.ts

 it("should render weather", () => {
    component.city = MOCK_WEATHER;
    fixture.detectChanges();
    const weatherCard = fixture.debugElement.nativeElement;
    expect(weatherCard.querySelector("div").textContent).toContain(
      MOCK_WEATHER.name
    );
});
```

## Plugin -ngx-markdown

This plugin was included to both demonstrate the ability to import and use other libraries and to add another route into the app, demonstrating lazy-loading
