import { Component, OnInit, OnDestroy } from "@angular/core";
import { Store } from "@ngrx/store";
import { WeatherDataService } from "./modules/weather-module/weather-data.service";
import { Weather } from "./modules/weather-module/model";
import { WeatherActions } from "./modules/weather-module/store/types";
import { AppState } from "./store/app.state";
import { Subscription } from "rxjs";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"]
})
export class AppComponent implements OnInit, OnDestroy {
  constructor(
    private _store: Store<AppState>,
    private wdataServ: WeatherDataService
  ) {}

  private sub: Subscription = null;

  ngOnInit() {
    // On load of the application populate the weather data.
    // This is done for the whole application as it's used by multiple routes
    this.sub = this.wdataServ.getCurrentGroupWeather().subscribe(
      data => {
        this._store.dispatch({
          type: WeatherActions.UPDATE_CITIES,
          payload: <Weather[]>data.list
        });
      },
      err => console.log(`error contating api: `, err)
    );
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }
}
