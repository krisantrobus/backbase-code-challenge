import { initialWeatherState } from "../modules/weather-module/store/state/weather.state";

export interface AppState {
    weather: any,
    loading: boolean,
}

export const initialAppState: AppState = {
    weather: initialWeatherState,
    loading: false,
}