export enum AppActions {
    TOGGLE_LOADING = 'TOGGLE_LOADING',
}

export interface Action {
    type: String;
    payload: any;
}