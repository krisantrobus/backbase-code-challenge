import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WeatherHomeComponent } from './pages/weather-home/weather-home.component';
import { HttpClientModule } from "@angular/common/http";
import { WeatherDataService } from './weather-data.service';
import { WeatherCardComponent } from './components/weather-card/weather-card.component';
import { StoreModule } from '@ngrx/store';
import { weatherReducer } from './store/reducers/weather.reducer';
import { CardHoverModule } from '../../shared/directives/card-hover/card-hover.module';
import { CityModalComponent } from './components/city-modal/city-modal.component';
import { WeatherRoutingModule } from './weather-routing.module';
import { HourlyForecastCardComponent } from './components/hourly-forecast-card/hourly-forecast-card.component';

@NgModule({
    imports: [
        CommonModule,
        HttpClientModule,
        CardHoverModule,
        WeatherRoutingModule,
        StoreModule.forFeature('weather', weatherReducer)
    ],
    providers: [WeatherDataService],
    declarations: [WeatherHomeComponent, WeatherCardComponent, CityModalComponent, HourlyForecastCardComponent, HourlyForecastCardComponent]
})

export class WeatherModule { }
