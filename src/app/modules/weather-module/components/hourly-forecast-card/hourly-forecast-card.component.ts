import { Component, OnInit, Input } from "@angular/core";
import { ForecastObj } from "../../model";

@Component({
  selector: "weather-hourly-forecast-card",
  templateUrl: "./hourly-forecast-card.component.html",
  styleUrls: ["./hourly-forecast-card.component.scss"]
})
export class HourlyForecastCardComponent implements OnInit {
  @Input() private forecast: ForecastObj = null;

  rain_chance: string = "0";

  constructor() {}

  ngOnInit() {
    // Set the rain chace. Has to be calculated on init otherwise the forecast variable would be null.
    // Value won't change during the lifecycle of this component.
    this.rain_chance = `${
      this.forecast && this.forecast.rain && this.forecast.rain["3h"]
        ? Math.round(this.forecast.rain["3h"] * 100)
        : 0
    }%`;
  }
}
