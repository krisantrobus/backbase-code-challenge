import {
  Component,
  OnInit,
  HostListener,
  ElementRef,
  OnDestroy
} from "@angular/core";
import {
  trigger,
  state,
  style,
  transition,
  animate
} from "@angular/animations";
import { Weather, WeatherForecast } from "../../model";
import { Store } from "@ngrx/store";
import { AppState } from "../../../../store/app.state";
import { Subscription } from "rxjs";
import {
  getSelectedCity,
  getSelectedCityForecastCurrentDay
} from "../../store/selectors/weather.selector";

@Component({
  selector: "weather-city-modal",
  templateUrl: "./city-modal.component.html",
  styleUrls: ["./city-modal.component.scss"],
  animations: [
    trigger("openClose", [
      state(
        "open",
        style({
          visibility: "visible",
          opacity: 1
        })
      ),
      state(
        "closed",
        style({
          visibility: "hidden",
          opacity: 0
        })
      ),
      transition("open => *", [animate(200)]),
      transition("closed => open", [animate(200)])
    ])
  ]
})
export class CityModalComponent implements OnInit, OnDestroy {
  constructor(private el: ElementRef, private _store: Store<AppState>) {}

  public active: boolean = false;
  public city: Weather = null;
  public forecast: WeatherForecast = null;
  private subCity: Subscription = null;
  private subForecast: Subscription = null;

  ngOnInit() {
    this.subCity = this._store
      .select(getSelectedCity)
      .subscribe(city => (this.city = city));

    this.subForecast = this._store
      .select(getSelectedCityForecastCurrentDay)
      .subscribe(forecast => (this.forecast = forecast));
  }

  ngOnDestroy() {
    this.subCity.unsubscribe();
    this.subForecast.unsubscribe();
  }

  // Toggle showing of card based on the active variable
  public toggleShow() {
    this.active = !this.active;
  }

  // This was to hide the modal on click out of element when active.
  // Didn't get working (registers clicks outside correctly) but toggle active shows
  // first then immediatly collapses
  @HostListener("document:click", ["$event"])
  clickout(event) {
    if (!this.el.nativeElement.contains(event.target) && this.active) {
      // this.active = false
    }
  }
}
