// Enumerator to define all actions that can be dispatched

export enum WeatherActions {
  SELECT_CITY = "SELECT_CITY",
  UPDATE_CITIES = "UPDATE_CITIES",
  UPDATE_FORECAST = "UPDATE_FORECAST"
}
