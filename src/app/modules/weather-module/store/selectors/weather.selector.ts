import { AppState } from "../../../../store/app.state";
import { createSelector } from "@ngrx/store";
import { WeatherState } from "../state/weather.state";
import { ForecastObj } from "../../model";

const selectWeather = (state: AppState) => state.weather;

export const getCitiesList = createSelector(
  selectWeather,
  (state: WeatherState) => state.cities
);

export const getSelectedCity = createSelector(
  selectWeather,
  (state: WeatherState) => state.selectedCity
);

export const getSelectedCityForecastCurrentDay = createSelector(
  selectWeather,
  (state: WeatherState) => {
    if (state.selectedCityForecast) {
      //Assemble a similar string to do acomparison to filter teh list array
      // for only the current day
      const currentDay = new Date();
      const year = currentDay.getFullYear(),
        month = ("0" + (currentDay.getMonth() + 1)).slice(-2),
        day = ("0" + currentDay.getDate()).slice(-2);

      const currentDayForecast: ForecastObj[] = state.selectedCityForecast.list.filter(
        (forecast: ForecastObj) => {
          // Only return current day forecast objects
          return (
            forecast.dt_txt.slice(0, 10) === `${year}-${month}-${day}` &&
            forecast
          );
        }
      );

      return { ...state.selectedCityForecast, list: currentDayForecast };
    }

    return state.selectedCityForecast;
  }
);
