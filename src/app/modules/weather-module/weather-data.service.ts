import { Injectable } from "@angular/core";
import { HttpHeaders, HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";

// A serice to contact the openweathermap API
@Injectable()
export class WeatherDataService {
  constructor(private http: HttpClient) {}

  // Vairables to store tockens.
  // Normally I'd store these as cookies for accessing on authentication
  _apiKey: string = "appid=2d175615a64d88955a9bc09b387ff814";

  _headers = new HttpHeaders({
    "Content-Type": "application/json"
  });

  // Store the api url so it can be used by many methods
  _apiUrl: string = "http://api.openweathermap.org/data/2.5";

  // use the HttpsClient to call the api and return an Observable
  public getCurrentGroupWeather(): Observable<any> {
    return this.http.get(
      `${
        this._apiUrl
      }/group?id=2643743,2653822,2988507,6545310,2759794&units=metric&${
        this._apiKey
      }`
    );
  }

  // Call the api to return the forecast for a city using city id
  public getHourlyDataForCityId(cityId: number): Observable<any> {
    return this.http.get(
      `${this._apiUrl}/forecast?id=${cityId}&units=metric&${this._apiKey}`
    );
  }
}
