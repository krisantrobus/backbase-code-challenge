import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MapHomeComponent } from './pages/map-home/map-home.component';

const routes: Routes = [
  { path: "", redirectTo: "/map/all", pathMatch: "full" },
  { path: "all", component: MapHomeComponent }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
  ],
  exports: [RouterModule],
})
export class MapRoutingModule { }

