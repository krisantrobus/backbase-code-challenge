import { Component, OnInit } from "@angular/core";
import { tileLayer, latLng, Coords, marker, Icon, icon } from "leaflet";
import { Subscription } from "rxjs";
import { Store } from "@ngrx/store";
import { AppState } from "src/app/store/app.state";
import { getCitiesList } from "src/app/modules/weather-module/store/selectors/weather.selector";
import { Weather } from "src/app/modules/weather-module/model";

@Component({
  selector: "weather-map-home",
  templateUrl: "./map-home.component.html",
  styleUrls: ["./map-home.component.scss"]
})
export class MapHomeComponent implements OnInit {
  constructor(private _store: Store<AppState>) {}

  sub: Subscription = null;
  citiesData: Weather[] = null;
  selectedCity: Weather = null;
  layers: any[] = new Array();
  options: Object = {
    layers: [
      tileLayer(
        "https://{s}.basemaps.cartocdn.com/light_all/{z}/{x}/{y}{r}.png",
        {
          attribution:
            '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors &copy; <a href="https://carto.com/attributions">CARTO</a>',
          subdomains: "abcd",
          maxZoom: 19
        }
      )
    ],
    zoom: 6,
    center: latLng(50.526, 5.2551)
  };
  map: L.Map = null;

  ngOnInit() {
    // set an event listener for when window resizes to determine the zoom of the map
    window.addEventListener("resize", () => this.changeZoom());

    this.sub = this._store.select(getCitiesList).subscribe(cities => {
      if (cities) {
        this.citiesData = cities;

        // Loop through the array of data and create a marker to display the data
        cities.forEach(weather => {
          this.layers.push(
            marker([weather.coord.lat, weather.coord.lon])
              .bindPopup(this.createPopup(weather))
              .setIcon(
                icon({
                  iconUrl: `http://openweathermap.org/img/w/${
                    weather.weather[0].icon
                  }.png`,
                  iconSize: [50, 50],
                  iconAnchor: [25, 25]
                })
              )
          );
        });
      }
    });
  }

  // The only way I cold implement custom html inside the popup. Could not use the componenets already made as
  // the inputs wouldn't pass in
  createPopup(weather: Weather) {
    return `
    <div
        class="justify-content-center text-center flex-column d-flex row"
        style="width: 300px;"
      >
        <i class="head-icon wi wi-owm-${
          weather.weather[0].id
        }" style="font-size:4rem; margin:0.25rem;"></i>
        <p class="small mt-3">${weather.weather[0].description}</p>

        <h4>${weather.name}</h4>
        <h5>${weather.main.temp}&#8451;</h5>
        <div class="row weather-meta justify-content-around mt-4 text-center">
          <div class="col-6 d-flex flex-column">
            <p>High: ${weather.main.temp_max}&#8451;</p>
            <p>Low: ${weather.main.temp_min}&#8451;</p>
          </div>
          <div class="col-6 d-flex flex-column">
            <p><i class="wi-cloudy-gusts wi"></i> ${weather.wind.speed}mph</p>
            <p><i class="wi-humidity wi"></i> ${weather.main.humidity}</p>
          </div>
        </div>
      </div>`;
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  // When the map has rendered set a variable so the map can be interacted with
  // programatically.
  // On first render of teh map call the zoom function to set correct zoom level
  onMapReady(map: L.Map): void {
    this.map = map;
    this.changeZoom();
  }

  // zoom out of the map if the window width is that of a mobile
  private changeZoom() {
    if (window.innerWidth < 571) {
      this.map.setZoom(4);
    } else {
      this.map.setZoom(6);
    }
  }
}
