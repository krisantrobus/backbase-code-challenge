import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { Routes, RouterModule } from "@angular/router";
import { HttpClientModule } from "@angular/common/http";
import { StoreModule } from "@ngrx/store";
import { MapHomeComponent } from "./pages/map-home/map-home.component";
import { CardHoverModule } from "../../shared/directives/card-hover/card-hover.module";
import { weatherReducer } from "../weather-module/store/reducers/weather.reducer";
import { MapRoutingModule } from "./map-routing.module";
import { LeafletModule } from "@asymmetrik/ngx-leaflet";

const routes: Routes = [
  { path: "", redirectTo: "/map/all", pathMatch: "full" },
  { path: "all", component: MapHomeComponent }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    HttpClientModule,
    CardHoverModule,
    MapRoutingModule,
    LeafletModule,
    StoreModule.forFeature("weather", weatherReducer)
  ],
  providers: [],
  declarations: [MapHomeComponent]
})
export class MapModule {}
