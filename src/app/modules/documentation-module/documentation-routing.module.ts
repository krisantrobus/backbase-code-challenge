import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { DocumentationComponent } from "./documentation/documentation.component";

const routes: Routes = [
  { path: "", redirectTo: "/documentation/default", pathMatch: "full" },
  { path: "default", component: DocumentationComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DocumentationRoutingModule {}
