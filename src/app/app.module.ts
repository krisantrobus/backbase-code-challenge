import { BrowserModule } from "@angular/platform-browser";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { NgModule } from "@angular/core";
import { StoreModule } from "@ngrx/store";

import { AppComponent } from "./app.component";
import { HeaderComponent } from "./shared/components/header/header.component";
import { AppRoutingModule } from "./app-routing.module";
import { appReducer } from "./store/app.reducer";
import { WeatherDataService } from "./modules/weather-module/weather-data.service";
import { HttpClientModule } from "@angular/common/http";
import { weatherReducer } from "./modules/weather-module/store/reducers/weather.reducer";

@NgModule({
  declarations: [AppComponent, HeaderComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    StoreModule.forRoot(appReducer),
    StoreModule.forFeature("weather", weatherReducer)
  ],
  providers: [WeatherDataService],
  bootstrap: [AppComponent]
})
export class AppModule {}
