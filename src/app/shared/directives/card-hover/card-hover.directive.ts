import { Directive, ElementRef, HostListener } from "@angular/core";

@Directive({
  selector: "[cardHover]"
})
export class CardHoverDirective {
  constructor(private el: ElementRef) {}

  // run when dovering over the element
  @HostListener("mouseenter") onMouseEnter() {
    this.highlight(true);
  }

  // run when leaving the element
  @HostListener("mouseleave") onMouseLeave() {
    this.highlight(false);
  }

  // Highlighted based on whether the mouse is over the element by applying styling
  private highlight(active: boolean): void {
    this.el.nativeElement.style.top = active ? "-10px" : "0";
    this.el.nativeElement.style.cursor = active ? "pointer" : "none";
  }
}
