import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardHoverDirective } from './card-hover.directive';

@NgModule({
  declarations: [CardHoverDirective],
  exports: [CardHoverDirective],
  imports: [
    CommonModule
  ]
})
export class CardHoverModule { }
