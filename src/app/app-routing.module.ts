import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

const routes: Routes = [
  { path: "", redirectTo: "/weather/all", pathMatch: "full" },
  {
    path: "weather",
    loadChildren: "./modules/weather-module/weather.module#WeatherModule"
  },
  {
    path: "map",
    loadChildren: "./modules/map-module/map.module#MapModule"
  },
  {
    path: "documentation",
    loadChildren:
      "./modules/documentation-module/documentation.module#DocumentationModule"
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
